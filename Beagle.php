<?php
/**
 * This file is a rewrite of the Beagle Project MediaWiki skin:
 *   http://web.archive.org/web/20090106025017/http://beagle-project.org/
 *
 * Original credits:
 *  Beagle - modified from MonoBook
 *  by: Joe Shaw (joeshaw@novell.com)
 *  license: TBD, probably GPL 
 *
 * Default artwork used:
 *  "Beagle puppy's head", extracted by Marcin Cieślak
 *  out of the photography by Wikimedia Commons user Juanelverdolaga
 *  https://commons.wikimedia.org/wiki/File:Beagle_puppy%27s_head.png
 *
 * Availabe under Creative Commons Attribution-Share Alike 4.0:
 *  https://creativecommons.org/licenses/by-sa/4.0/deed.en
 */

/**
 * Inherit main code from SkinTemplate, set the CSS and template filter.
 * @ingroup Skins
 */
class SkinBeagle extends SkinTemplate {
	var $skinname = 'beagle', $stylename = 'beagle',
		$template = 'BeagleTemplate', $useHeadElement = true;

	public function initPage( OutputPage $out ) {
		global $wgBeagleImage;
		parent::initPage( $out );
		$out->addInlineStyle( "#header h1 { background: transparent " .
			"url(" .  ( $wgBeagleImage ?: 
				$this->getSkinStylePath( '/bigielbanner.png' ) . ") " .
			"no-repeat; }" ) );

		$out->addModuleStyles( array(
			'skins.beagle.css'
		) );
	}
	/**
	 * Add CSS via ResourceLoader
	 *
	 * @param $out OutputPage
	 */
	function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );
	}
}


/**
 * @todo document
 * @addtogroup Skins
 */
class BeagleTemplate extends BaseTemplate {

	function sel( $sel ) {
		if ( $sel ) {
			if ( $sel[0] == '#' )
				return ' id="' . substr( $sel, 1 ) . '"';
			else
				return ' class="' . $sel . '"';
		} else
			return "";
	}

	function div ( $sel = false, $role = false) {
		print "<div" . $this->sel( $sel );
		if ( $role ) print " role=\"$role\"";
		print ">";
	}

	function div_ends ( $sel = false ) {
		print "</div>\n";
	}

	function div_html( $dataitem, $sel ) {
		if ( $this->data[ $dataitem ] ) {
           $this->div( $sel );
		   $this->html( $dataitem );
		   $this->div_ends( $sel );
		}
	}

	function div_span_html( $dataitem, $sel, $span = false ) {
		if ( $this->data[ $dataitem ] ) {
           $this->div( $sel );
			   if ( $span ) { print "<span" . $this->sel( $span ) . ">" ; }
				   $this->html( $dataitem );
			   if ( $span ) { print "</span>"; }
		   $this->div_ends(  $sel );
		}
	}

	function a_href_msg( $target_id, $msg ) {
		print " <a href=\"$target_id\">";
		$this->msg( $msg );
		print "</a>";
	}

	function jumplinks() {
		if ( $this->data['showjumplinks'] ) {
			$this->div( '#jump-to-nav' );
				$this->msg( 'jumpto' );	
				$this->a_href_msg( '#nav_top', 'jumptonavigation' );
				print ",";
				$this->a_href_msg( '#searchInput', 'jumptosearch' );
			$this->div_ends( '#jump-to-nav' );
		}
	}

	function data_html( $dataitem ) {
		if ( $this->data[ $dataitem ] )
			$this->html( $dataitem );
	}

	function data_rem( $dataitem ) {
		if ( $this->data[ $dataitem ] ) {
			print "<!-- Debug output:\n";
			$this->text( $dataitem );
			print "\n-->\n";
		}
	}

	function ul_list( $source, $sel, $last_item = false ) { 
		if ( $source && is_array( $source ) ) {
			print "<ul" . $this->sel( $sel ) . ">\n";
			foreach( $source as $key => $action ) {
				print "\t" . $this->makeListItem( $key, $action ) . "\n";
			}
			if ( $last_item ) {
				print "\t" . $last_item . "\n";
			}
			print "</ul>\n";
		}
	}
	function ul_html ( $source ) { 
		if ( $source && is_array( $source ) ) {
			print "<ul class=\"nav-bottom\">\n";
			foreach( $source as $key ) {
				print "\t<li>";
				print $this->html( $key );
				print "</li>\n";
			}
			print "</ul>\n";
		}
	}

	function ul_plain ( $source ) { 
		if ( $source && is_array( $source ) ) {
			print "<ul class=\"nav-bottom\">\n";
			foreach( $source as $value ) {
				print "\t<li>";
				print $value;
				print "</li>\n";
			}
			print "</ul>\n";
		}
	}

	function nav_top( $source ) {
		print "<nav>\n";
		$this->ul_list( $source, '#nav-top', '<li class="cheat"></li>' );
		print "</nav>\n";
	}

	function nav_bottom( $source ) {
		print "<nav>\n";
		$this->ul_list( $source, 'nav-bottom' );
		print "</nav>\n";
	}

	function recent_changes_link() {
		$d = array();
		$d[ 'id' ] = 'nn-recentchanges';
		$d[ 'text' ] = $this->getMsg( 'recentchanges' ); 
		$rc = $this->getMsg( 'recentchanges-url' )->inContentLanguage()->text();
		$title = Title::newFromText( $rc );
		if ( $title ) {
			$d[ 'href' ] = $title->getLinkURL();
			return $d;
		} else {
			return false;
		}
	}

	function main_bar_source() {
		$sidebar = $this->getSidebar();
		if ( $sidebar ) 
			if ( isset( $sidebar[ 'navigation' ] ) )
				if ( isset( $sidebar[ 'navigation' ][ 'content' ] ) )
					return $sidebar[ 'navigation' ][ 'content' ];
		return false;
	}

	function special_pages_source() {
		$source = $this->getToolbox();
		$source [ 'recentchanges' ] = $this->recent_changes_link();
		return $source;
	}

	function page_info() {
		$this->div(   '#article_info', 'contentinfo' );
			$this->data_html( 'lastmod' );
			$this->data_html( 'viewcount' );
		$this->div_ends( '#article_info ');
	}

	function footer_icons() {
		$source = array();
		foreach ( $this->getFooterIcons( 'icononly' ) as $blockName => $footerIcons )
			foreach ( $footerIcons as $icon ) 
				array_push ( $source, $this->getSkin()->makeFooterIcon( $icon ) );
		return $source;
	}

	function search_form() { ?>
<div class="searchForm" role="search">
	<form id="searchForm" class="searchForm" name="searchForm" action="<?php $this->text('searchaction') ?>">
		<input type="hidden" name="title" value="<?php $this->text( 'searchtitle' ) ?>" />
		<?php print	$this->makeSearchInput(); ?>

		<?php print	$this->makeSearchButton( 'go' ); ?>

		<?php print	$this->makeSearchButton( 'fulltext' ); ?>

	</form>
</div>
<?php
	} 

	function dotted_spacing() {
		print "<hr class=\"clear dotted spacing\" />\n";
	}

	function execute() {
		$skin = $this->getSkin();
		$body = $this->data['bodycontent'];

		$skin_path = $this->data['stylepath'].'/'.$this->data['stylename'];

		// Output HTML Page
		$this->html( 'headelement' );

?><!-- START main page container -->
<div id="pagecontainer">

<!-- START subsection header and subnav -->
<div id="header">
	<h1><?php $this->text( 'pagetitle' ) ?></h1>
</div>
<hr class="clear spacing" />
<?php
	$this->nav_top( $this->main_bar_source() );
?>
<div id="content" role="main">
<div id="content-body">
<div id="articleBody">
<a id="top"></a>
<?php		$this->div_html( 'sitenotice', '#siteNotice' ); ?>
<div id="articleContent">
<?php		
			$this->div_html( 'subtitle', '#contentSub'  );   
			$this->div_html( 'undelete', '#contentSub2' );   
			$this->div_html( 'newtalk',  '#usermessage' );   
			$this->jumplinks();
			$this->html( 'bodytext' ); 
			$this->data_html(   'catlinks' );
?>
</div><!-- aricleContent -->
</div><!-- articleBody -->
</div><!-- content-body -->
<hr class="clear" />
<?php	
		print $this->getIndicators();
		$this->page_info();     
?>
</div><!-- content -->
<?php 
		$this->dotted_spacing();
		$this->html( 'dataAfterContent' );

		$this->nav_bottom( $this->getPersonalTools() );
		$this->nav_bottom( $this->data[ 'content_actions' ] );
		$this->nav_bottom( $this->data[ 'language_urls' ] );
		$this->search_form();
		$this->nav_bottom( $this->special_pages_source() );
		Hooks::Run( 'SkinTemplateToolboxEnd', array( &$this ) );
		# $this->ul_html( $this->getFooterLinks( )[ 'places' ] );
		$this->ul_plain( $this->footer_icons() );
		$this->div();
		$this->dotted_spacing();
		$this->div_ends();
		$this->html( 'reporttime' );
		$this->data_rem( 'debug' );
		$this->printTrail();   ?>
</div>
</body>
</html>
<?php
	}
}
